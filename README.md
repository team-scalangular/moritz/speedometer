# Speedometer

Generated with [avr-hal-template](https://github.com/Rahix/avr-hal-template.git)

# Build and deploy

First connect the arduino via its usb serial.


## Option 1: Manually

```bash
cargo build --release
```

creates the `.elf`-File which can be deployed with

```bash
sudo avrdude -p m328p -c arduino -P /dev/{TTYPORT} -b 57600 -U flash:w:target/avr-atmega328p/release/{ELF-FILE}
```

where `{TTYPORT}` is the port of the connected arduino (run `ls /dev/tty*` to find it) and `{ELF-FILE}` is the filename
of the generated `.elf`-File.


## Option 2: Ravedude

If you don't have them already, install `ravedude`:

```bash
cargo install ravedude
```

Run

```bash
cargo run
```

to build and deploy it to the board. (untested)