#[derive(Clone)]
pub struct BikeConfig {
    pub circumference_in_mm: f64,

}

pub const fn create_bike() -> BikeConfig {
    // let inches = 27.0;
    // let radius_in_mm = inches * 25.4 / 2.0;
    // let circumference_in_mm = 2.0 * (PI as f64) * radius_in_mm;

    BikeConfig {
        circumference_in_mm: 2154.50424183,
    }
}

pub const fn create_bike_from_circumference_in_mm(circumference_in_mm: f64) -> BikeConfig {
    BikeConfig {
        circumference_in_mm,
    }
}
