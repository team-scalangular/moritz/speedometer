use heapless::{Deque};
use crate::bike::BikeConfig;

const DEQUE_SIZE: usize = 120;

#[derive(Clone)]
pub struct Speedometer {
    time_values: Deque<u64, DEQUE_SIZE>,
    bike: BikeConfig,
}

pub const fn new(bike: BikeConfig) -> Speedometer {
    Speedometer {
        time_values: Deque::new(),
        bike,
    }
}

impl Speedometer {
    pub fn add_value(&mut self, time_since_start: u64) {
        self.remove_last_if_full();

        self.time_values.push_front(time_since_start).expect("should be able to push value into deque");
    }

    pub fn velocity_in_m_per_s_in(&self, start: u64, end: u64) -> f64 {
        let rotation_time_in_ms = self.average_delta_in_interval(start, end);

        // <= 0.0 doesnt work! needs to be < 0.000...1
        if rotation_time_in_ms < 0.0001 {
            0.0
        } else {
            self.bike.circumference_in_mm / rotation_time_in_ms
        }
    }

    fn remove_last_if_full(&mut self) {
        if self.time_values.len() >= DEQUE_SIZE {
            self.time_values.pop_back();
        }
    }

    fn is_in_interval(start: u64, end: u64, time_since_start: u64) -> bool {
        time_since_start as u64 <= start && time_since_start as u64 >= end
    }

    fn average_delta_in_interval(&self, start: u64, end: u64) -> f64 {
        let values = self.time_values
            .iter()
            .filter(|time_and_delta| Self::is_in_interval(start, end, **time_and_delta))
            .count();

        ((start - end) as f64) / (values as f64)
    }
}
