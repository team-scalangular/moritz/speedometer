#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]


use core::ops::Deref;

use defmt::{info, unwrap};
use embassy_executor::Spawner;
use embassy_rp::gpio::{Input, Level, Output, Pull};
use embassy_rp::Peripherals;
use {defmt_rtt as _, panic_probe as _};
use embassy_rp::peripherals::{PIN_14, PIN_15};
use embassy_sync::blocking_mutex::raw::ThreadModeRawMutex;
use embassy_sync::mutex::Mutex;
use embassy_time::{Duration, Instant, Timer};

use crate::bike::create_bike;
use crate::speedometer::Speedometer;

pub mod speedometer;

pub mod bike;

static MUTEX: Mutex<ThreadModeRawMutex, Speedometer> = Mutex::new(speedometer::new(create_bike()));

#[embassy_executor::task]
async fn measure_speed(mut led: Output<'static, PIN_15>, mut hall_input: Input<'static, PIN_14>) {
    let start = Instant::now();

    loop {
        // todo: change to correct when hall is there
        hall_input.wait_for_rising_edge().await;
        led.toggle();

        let time_at_interrupt = Instant::now();
        let delta_to_start = (time_at_interrupt - start).as_millis();
        info!("Received Interrupt with delta to start of {}ms", delta_to_start);

        {
            let mut guard = MUTEX.lock().await;
            guard.add_value(delta_to_start);
        }
    }
}

#[embassy_executor::task]
async fn display() {
    loop {
        let current_loop_time = Instant::now().as_millis();
        {
            let speedometer = MUTEX.lock().await;

            if current_loop_time > 1000 {
                let current_velocity = speedometer.velocity_in_m_per_s_in(current_loop_time, current_loop_time - 1000);
                info!("velocity (last 1000ms) {}km/h", (current_velocity * 3.6));
            }

            if current_loop_time > 5000 {
                let last5seconds_mutability = speedometer.velocity_in_m_per_s_in(current_loop_time, current_loop_time - 5000);
                info!("velocity [0s-5s] {}km/h", (last5seconds_mutability * 3.6));
            }
        }

        // TODO: when actual display is updated we might not night this
        Timer::after(Duration::from_millis(500)).await;
    }
}


#[embassy_executor::main]
async fn main(spawner: Spawner) {
    info!("Program start");
    let p: Peripherals = embassy_rp::init(Default::default());

    // These are implicitly used by the spi driver if they are in the correct mode
    // let _spi_sclk = pins.gpio6.into_mode::<hal::gpio::FunctionSpi>();
    // let _spi_mosi = pins.gpio7.into_mode::<hal::gpio::FunctionSpi>();
    // let _spi_miso = pins.gpio4.into_mode::<hal::gpio::FunctionSpi>();
    // let spi = hal::Spi::<_, _, 8>::new(pac.SPI0);
    //
    // let mut lcd_led = pins.gpio12.into_push_pull_output();
    // let dc = pins.gpio13.into_push_pull_output();
    // let rst = pins.gpio14.into_push_pull_output();
    //
    // // Exchange the uninitialised SPI driver for an initialised one
    // let spi = spi.init(
    //     &mut pac.RESETS,
    //     clocks.peripheral_clock.freq(),
    //     RateExtU32::Hz(16_000_000u32),
    //     &embedded_hal::spi::MODE_0,
    // );
    //
    // let mut disp = st7735_lcd::ST7735::new(spi, dc, rst, true, false, 160, 128);
    //
    // disp.init(&mut delay).unwrap();
    // disp.set_orientation(&Orientation::Landscape).unwrap();
    // disp.clear(Rgb565::BLACK).unwrap();
    // disp.set_offset(0, 25);
    // disp.init(&mut delay).unwrap();
    // disp.set_orientation(&Orientation::Landscape).unwrap();
    // disp.clear(Rgb565::BLACK).unwrap();
    // disp.set_offset(0, 25);
    //
    // let character_style = MonoTextStyle::new(&FONT_6X10, Rgb565::RED);
    // let timer = hal::Timer::new(pac.TIMER, &mut pac.RESETS);
    //
    // // Wait until the background and image have been rendered otherwise
    // // the screen will show random pixels for a brief moment
    // lcd_led.set_high().unwrap();

    let hall = Input::new(p.PIN_14, Pull::Down); // currently a button
    let led = Output::new(p.PIN_15, Level::Low);

    info!("spawning tasks");

    unwrap!(spawner.spawn(measure_speed(led, hall)));
    unwrap!(spawner.spawn(display()));
}
